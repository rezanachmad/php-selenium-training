<?php

require_once dirname(__FILE__).'/../Selenium.php';

/**
 * 
 *
 * @author Rezan Achmad <rezan.achmad@gdpventure.com>
 */
class RegisterTest extends Selenium
{

	/**
	 * @param string $formData
	 * @dataProvider providerSuccess
	 */
	public function testSuccess($formData)
	{
		$this->cleanUpDB();
		
		$this->url('http://localhost/selenium/');
		$this->element($this->using('link text')->value('Register'))->click();
		$this->element($this->using('id')->value('User_fullname'))->value($formData['fullname']);
		$this->element($this->using('name')->value('User[email]'))->value($formData['email']);
		$this->element($this->using('class name')->value('password'))->value($formData['password']);
		$this->element($this->using('css selector')->value('#User_retypePassword'))->value($formData['retypePassword']);
		$this->element($this->using('id')->value('User_address'))->value($formData['address']);
		$this->select($this->element($this->using('id')->value('User_country')))->selectOptionByValue($formData['country']);
		if($formData['agree'])
		{
			$this->element($this->using('id')->value('User_agree'))->click();
		}
		$this->element($this->using('id')->value('submit'))->click();
		
		// Sleep to see the result
		sleep(5);
	}

	/**
	 * @return array
	 */
	public function providerSuccess()
	{
		return array(
			array(
				array(
					'fullname'=>'PHP Selenium',
					'email'=>'php.selenium@mail.com',
					'password'=>'password',
					'retypePassword'=>'password',
					'address'=>'Slipi',
					'country'=>'ID',
					'agree'=>true,
				),
			)
		);
	}

}

?>
