<?php

require_once dirname(__FILE__).'/../Selenium.php';

/**
 * 
 *
 * @author Rezan Achmad <rezan.achmad@gdpventure.com>
 */
class RegisterTest extends Selenium
{

	/**
	 * @param string $formData
	 * @dataProvider providerSuccess
	 */
	public function testSuccess($formData)
	{
		$this->cleanUpDB();
		
		$this->url('http://localhost/selenium/');
		$this->byLinkText('Register')->click();
		$this->byId('User_fullname')->value($formData['fullname']);
		$this->byName('User[email]')->value($formData['email']);
		$this->byClassName('password')->value($formData['password']);
		$this->byCssSelector('#User_retypePassword')->value($formData['retypePassword']);
		$this->byId('User_address')->value('Wrong Address');
		$this->byId('User_address')->clear();
		$this->byId('User_address')->value($formData['address']);
		$this->select($this->byId('User_country'))->selectOptionByValue($formData['country']);
		if($formData['agree'])
		{
			$this->byId('User_agree')->click();
		}
		$this->byId('submit')->click();
		
		sleep(5);
	}

	/**
	 * @return array
	 */
	public function providerSuccess()
	{
		return array(
			array(
				array(
					'fullname'=>'PHP Selenium',
					'email'=>'php.selenium@mail.com',
					'password'=>'password',
					'retypePassword'=>'password',
					'address'=>'Slipi',
					'country'=>'ID',
					'agree'=>true,
				),
			)
		);
	}

}

?>
