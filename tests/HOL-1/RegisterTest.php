<?php

require_once dirname(__FILE__).'/../Selenium.php';

/**
 * 
 *
 * @author Rezan Achmad <rezan.achmad@gdpventure.com>
 */
class RegisterTest extends Selenium
{

	/**
	 * @param string $formData
	 * @dataProvider providerSuccess
	 */
	public function testSuccess($formData)
	{
		$this->cleanUpDB();
		
		// TODO 00: Please see providerSuccess(). It contain data that will be used.
		
		// TODO 01: go to http://localhost/selenium/
		
		// TODO 02: click 'Register' link using byLinkText('Register')
		
		// TODO 03: fill fullname using byId('User_fullname') with value from $formData['fullname']
		
		// TODO 04: fill email using byName('User[email]') with value from $formData['email']
		
		// TODO 05: fill password using byClassName('password') with value from $formData['password']
		
		// TODO 06: fill retype password using byCssSelector('#User_retypePassword') with value from $formData['retypePassword']
		
		$this->byId('User_address')->value('Wrong Address');
		
		// TODO 07: clear the value of address
		
		$this->byId('User_address')->value($formData['address']);
		
		// TODO 08: select country, use selectOptionByValue() then fill it with $formData['country']
		
		if($formData['agree'])
		{
			$this->byId('User_agree')->click();
		}
		
		// TODO 09: click submit button
		
		sleep(5);
	}

	/**
	 * @return array
	 */
	public function providerSuccess()
	{
		return array(
			array(
				array(
					'fullname'=>'PHP Selenium',
					'email'=>'php.selenium@mail.com',
					'password'=>'password',
					'retypePassword'=>'password',
					'address'=>'Slipi',
					'country'=>'ID',
					'agree'=>true,
				),
			)
		);
	}

}

?>
