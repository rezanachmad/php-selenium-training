<?php

require_once dirname(__FILE__).'/Page.php';
require_once dirname(__FILE__).'/HomePage.php';

/**
 * 
 *
 * @author Rezan Achmad <rezan.achmad@gdpventure.com>
 */
class LoginPage extends Page
{
	const ID_FORM='login-form';
	const ID_EMAIL='LoginForm_email';
	const ID_PASSWORD='LoginForm_password';
	const ID_LOGIN_BUTTON='login';
	const WAIT_DURATION=10000;
	const MILLISECONDS=1000;

	public function loginUser($email,$password)
	{
		$this->enterEmail($email);
		$this->enterPassword($password);

		// TODO 01: Wait for login button shown. Just set implicitWait().
		// Use const WAIT_DURATION for amount of duration
		
		$this->clickLoginButton();

		// TODO 02: Wait for ajax.
		// Check if the login form disappear is enough.
		// Call waitForFormDisappear() to wait.

		return new HomePage($this->selenium);
	}

	/**
	 * @param string $email
	 */
	public function enterEmail($email)
	{
		$this->clearAndSetValue($this->selenium->byId(self::ID_EMAIL),$email);
		return $this;
	}

	/**
	 * @param string $password
	 */
	public function enterPassword($password)
	{
		$this->clearAndSetValue($this->selenium->byId(self::ID_PASSWORD),$password);
		return $this;
	}

	public function clickLoginButton()
	{
		$this->selenium->byId(self::ID_LOGIN_BUTTON)->click();
		return $this;
	}

	public function waitForFormDisappear()
	{
		$delay=100;
		$startTime=microtime(TRUE) * self::MILLISECONDS;
		$duration=0;
		$dissaper=false;

		// Reset implicit wait
		$this->selenium->timeouts()->implicitWait(0);
		
		while (!$dissaper && $duration < self::WAIT_DURATION)
		{
			try
			{
				$this->selenium->byId(self::ID_FORM);
			}
			catch (PHPUnit_Extensions_Selenium2TestCase_WebDriverException $exc)
			{
				$dissaper=true;
			}

			usleep($delay);
			$duration=microtime(TRUE) * self::MILLISECONDS - $startTime;
		}
		
		// Un Reset implicit wait
		$this->selenium->timeouts()->implicitWait(self::WAIT_DURATION);
	}

}

?>
