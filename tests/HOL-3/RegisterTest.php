<?php

require_once dirname(__FILE__).'/../Selenium.php';

/**
 * 
 *
 * @author Rezan Achmad <rezan.achmad@gdpventure.com>
 */
class RegisterTest extends Selenium
{

	/**
	 * @param string $formData
	 * @dataProvider providerSuccess
	 */
	public function testSuccess($formData)
	{
		$this->cleanUpDB();
		$this->url('http://localhost/selenium/register');
		
		$this->fillFormAndSubmit($formData);
		$this->login($formData['email'], $formData['password']);
		
		$this->url('http://localhost/selenium/user/update');
		$this->verifyProfile($formData);
		
		sleep(5);
	}

	/**
	 * @return array
	 */
	public function providerSuccess()
	{
		return array(
			array(
				array(
					'fullname'=>'PHP Selenium',
					'email'=>'php.selenium@mail.com',
					'password'=>'password',
					'retypePassword'=>'password',
					'address'=>'Slipi',
					'country'=>'ID',
					'agree'=>true,
				),
			)
		);
	}
	
	/**
	 * @param array $formData
	 */
	protected function fillFormAndSubmit($formData)
	{
		$this->byId('User_fullname')->value($formData['fullname']);
		$this->byId('User_email')->value($formData['email']);
		$this->byId('User_password')->value($formData['password']);
		$this->byId('User_retypePassword')->value($formData['retypePassword']);
		$this->byId('User_address')->value($formData['address']);
		$this->select($this->byId('User_country'))->selectOptionByValue($formData['country']);
		if($formData['agree'])
		{
			$this->byId('User_agree')->click();
		}
		$this->byId('submit')->click();
	}
	
	/**
	 * @param string $username
	 * @param string $password
	 */
	protected function login($username, $password)
	{
		$this->byId('LoginForm_email')->value($username);
		$this->byId('LoginForm_password')->value($password);
		
		// Wait for login button shown
		sleep(2);
		
		$this->byId('login')->click();
		
		// Wait for AJAX
		sleep(2);
	}
	
	/**
	 * @param array $formData
	 */
	protected function verifyProfile($formData)
	{	
		// TODO 01: get fullname value then store to $fullname
		$this->assertEquals($formData['fullname'],$fullname);
		
		// TODO 02: get email text then store to $email
		$this->assertEquals($formData['email'],$email);
		
		// TODO 03: get address value using attribute('value') then store to $address
		$this->assertEquals($formData['address'],$address);
		
		$country = $this->byId('User_country')->value();
		$this->assertEquals($formData['country'],$country);
	}

}

?>
