<?php

require_once dirname(__FILE__).'/../Selenium.php';
require_once dirname(__FILE__).'/../pages/RegisterPage.php';
require_once dirname(__FILE__).'/../pages/UpdateProfilePage.php';

/**
 * 
 *
 * @author Rezan Achmad <rezan.achmad@gdpventure.com>
 */
class RegisterTest extends Selenium
{

	/**
	 * @param string $formData
	 * @dataProvider providerSuccess
	 */
	public function testSuccess($formData)
	{
		$this->cleanUpDB();
		$this->urlRelative(self::REGISTER_PAGE);
		
		$register=new RegisterPage($this);
		$loginPage=$register->registerUser($formData);
		$this->assertEquals('Congratulations! You Have Successfully Registered.',$loginPage->getFlashSuccess());

		$loginPage->loginUser($formData[RegisterPage::ID_EMAIL], $formData[RegisterPage::ID_PASSWORD]);
		
		$this->urlRelative(self::UPDATE_PROFILE_PAGE);
		$profilePage = new UpdateProfilePage($this);
		$profilePage->verifyProfile($formData);
		
		$profilePage->logout();
	}

	/**
	 * @return array
	 */
	public function providerSuccess()
	{
		return array(
			array(
				array(
					RegisterPage::ID_EMAIL=>'php.selenium@mail.com',
					RegisterPage::ID_FULLNAME=>'PHP Selenium',
					RegisterPage::ID_PASSWORD=>'password',
					RegisterPage::ID_RETYPE_PASSWORD=>'password',
					RegisterPage::ID_ADDRESS=>'Slipi',
					RegisterPage::ID_COUNTRY=>'ID',
					RegisterPage::ID_AGREE=>true,
				),
			)
		);
	}

}

?>
