<?php

require_once dirname(__FILE__).'/UserForm.php';
require_once dirname(__FILE__).'/LoginPage.php';

/**
 * 
 * @author Rezan Achmad <rezan.achmad@gdpventure.com>
 */
class RegisterPage extends UserForm
{
	const ID_PASSWORD='User_password';
	const ID_RETYPE_PASSWORD='User_retypePassword';
	const ID_AGREE='User_agree';
	const ID_REGISTER_BUTTON='submit';

	public function registerUser($formData)
	{
		$this->fillForm($formData);
		$this->clickRegisterButton();

		return new LoginPage($this->selenium);
	}

	/**
	 * @param string[] $formData
	 */
	public function fillForm($formData)
	{
		$this->enterFullName($formData[self::ID_FULLNAME]);
		$this->enterEmail($formData[self::ID_EMAIL]);
		$this->enterPassword($formData[self::ID_PASSWORD]);
		$this->enterRetypePassword($formData[self::ID_RETYPE_PASSWORD]);
		$this->enterAddress($formData[self::ID_ADDRESS]);
		$this->selectCountryByValue($formData[self::ID_COUNTRY]);
		if ($formData[self::ID_AGREE])
			$this->clickAgree();

		return $this;
	}

	/**
	 * @param string $email
	 */
	public function enterEmail($email)
	{
		$this->clearAndSetValue($this->selenium->byId(self::ID_EMAIL),$email);
		return $this;
	}

	/**
	 * @param string $password
	 */
	public function enterPassword($password)
	{
		$this->clearAndSetValue($this->selenium->byId(self::ID_PASSWORD),$password);
		return $this;
	}

	/**
	 * @param string $retypePassword
	 */
	public function enterRetypePassword($retypePassword)
	{
		$this->clearAndSetValue($this->selenium->byId(self::ID_RETYPE_PASSWORD),$retypePassword);
		return $this;
	}

	public function clickAgree()
	{
		$this->selenium->byId(self::ID_AGREE)->click();
		return $this;
	}

	public function clickRegisterButton()
	{
		$this->selenium->byId(self::ID_REGISTER_BUTTON)->click();
		return $this;
	}

	/**
	 * @param string $text
	 */
	public function verifyTextPresent($text)
	{
		$this->selenium->assertContains($text,$this->selenium->byId('user-form')->text());
	}

	/**
	 * @param string $id element ID
	 */
	public function removeMaxLengthAttribute($id)
	{
		$script=<<<JAVASCRIPT
	var element = document.getElementById('$id');
	element.removeAttribute('maxlength');
JAVASCRIPT;

		$this->selenium->execute(array(
			'script'=>$script,
			'args'=>array(),
		));
	}

}

?>
