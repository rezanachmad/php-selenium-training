<?php

require_once dirname(__FILE__).'/UserForm.php';

/**
 * 
 *
 * @author Rezan Achmad <rezan.achmad@gdpventure.com>
 */
class UpdateProfilePage extends UserForm
{
	const ID_SAVE_BUTTON='submit';

	public function verifyProfile($data)
	{
		$this->selenium->assertEquals($data[self::ID_EMAIL],$this->selenium->byId(self::ID_EMAIL)->text());
		$this->selenium->assertEquals($data[self::ID_FULLNAME],$this->selenium->byId(self::ID_FULLNAME)->value());
		$this->selenium->assertEquals($data[self::ID_ADDRESS],$this->selenium->byId(self::ID_ADDRESS)->value());
		$this->selenium->assertEquals($data[self::ID_COUNTRY],$this->selenium->byId(self::ID_COUNTRY)->value());

		return $this;
	}
	
	public function clickSaveButton()
	{
		$this->selenium->byId(self::ID_SAVE_BUTTON)->click();
		return $this;
	}

}

?>
