<?php

/**
 * Abstract of all pages
 *
 * @author Rezan Achmad <rezan.achmad@gdpventure.com>
 */
abstract class Page
{
	const CSS_FLASH_SUCCESS='div.flash-success';
	const CSS_LOGOUT_LINK='#mainmenu > ul > li:last-child > a';

	/**
	 * @var Selenium 
	 */
	protected $selenium;

	/**
	 * @param Selenium  $selenium
	 */
	public function __construct($selenium)
	{
		$this->selenium=$selenium;
	}

	/**
	 * @return string
	 */
	public function getFlashSuccess()
	{
		return $this->selenium->byCssSelector(self::CSS_FLASH_SUCCESS)->text();
	}

	public function logout()
	{
		$this->selenium->byCssSelector(self::CSS_LOGOUT_LINK)->click();
	}
	
	/**
	 * 
	 * @param PHPUnit_Extensions_Selenium2TestCase_Element $element
	 * @param string $value
	 */
	public function clearAndSetValue($element,$value)
	{
		$element->clear();
		$element->value($value);
	}
}

?>
