<?php

require_once dirname(__FILE__).'/Page.php';
require_once dirname(__FILE__).'/HomePage.php';

/**
 * 
 *
 * @author Rezan Achmad <rezan.achmad@gdpventure.com>
 */
class LoginPage extends Page
{
	const ID_EMAIL='LoginForm_email';
	const ID_PASSWORD='LoginForm_password';
	const ID_LOGIN_BUTTON='login';

	public function loginUser($email,$password)
	{
		$this->enterEmail($email);
		$this->enterPassword($password);
		
		// Wait for login button shown
		sleep(2);
		
		$this->clickLoginButton();
		
		// Wait for AJAX
		sleep(2);

		return new HomePage($this->selenium);
	}

	/**
	 * @param string $email
	 */
	public function enterEmail($email)
	{
		$this->clearAndSetValue($this->selenium->byId(self::ID_EMAIL),$email);
		return $this;
	}

	/**
	 * @param string $password
	 */
	public function enterPassword($password)
	{
		$this->clearAndSetValue($this->selenium->byId(self::ID_PASSWORD),$password);
		return $this;
	}

	public function clickLoginButton()
	{
		$this->selenium->byId(self::ID_LOGIN_BUTTON)->click();
		return $this;
	}

}

?>
