<?php

require_once dirname(__FILE__).'/Page.php';

/**
 * 
 *
 * @author Rezan Achmad <rezan.achmad@gdpventure.com>
 */
abstract class UserForm extends Page
{
	const ID_FULLNAME='User_fullname';
	const ID_EMAIL='User_email';
	const ID_ADDRESS='User_address';
	const ID_COUNTRY='User_country';

	/**
	 * @param string $fullName
	 */
	public function enterFullName($fullName)
	{
		$this->clearAndSetValue($this->selenium->byId(self::ID_FULLNAME),$fullName);
		return $this;
	}

	/**
	 * @param string $address
	 */
	public function enterAddress($address)
	{
		$this->clearAndSetValue($this->selenium->byId(self::ID_ADDRESS),$address);
		return $this;
	}

	/**
	 * @param string $country
	 */
	public function selectCountryByValue($country)
	{
		$this->selenium->select($this->selenium->byId(self::ID_COUNTRY))->selectOptionByValue($country);
		return $this;
	}

}

?>
