<?php

require_once dirname(__FILE__).'/../Selenium.php';
require_once dirname(__FILE__).'/../pages/LoginPage.php';
require_once dirname(__FILE__).'/../pages/UpdateProfilePage.php';

/**
 * 
 *
 * @author Rezan Achmad <rezan.achmad@gdpventure.com>
 */
class UpdateProfileTest extends Selenium
{

	/**
	 * @param string $email
	 * @param string $password
	 * @dataProvider providerChangeAddress
	 */
	public function testChangeAddress($data)
	{
		$this->urlRelative(self::LOGIN_PAGE);
		$loginPage=new LoginPage($this);
		$loginPage->loginUser($data[UpdateProfilePage::ID_EMAIL],$data[LoginPage::ID_PASSWORD]);

		$this->urlRelative(self::UPDATE_PROFILE_PAGE);
		$profilePage=new UpdateProfilePage($this);
		$profilePage->enterAddress('KS Tubun');
		$profilePage->clickSaveButton();

		// TODO 01: assert that alert text equals 'Are you sure?'
		
		// TODO 02: dismiss alert

		$profilePage->enterAddress($data[UpdateProfilePage::ID_ADDRESS]);
		$profilePage->clickSaveButton();
		
		// TODO 03: accept alert

		$profilePage->verifyProfile($data);
	}

	public function providerChangeAddress()
	{
		return array(
			array(
				array(
					UpdateProfilePage::ID_EMAIL=>'gdp.labs@mail.com',
					LoginPage::ID_PASSWORD=>'password',
					UpdateProfilePage::ID_FULLNAME=>'GDP Labs',
					UpdateProfilePage::ID_ADDRESS=>'KS Tubun 2C/8',
					UpdateProfilePage::ID_COUNTRY=>'ID',
				),
			),
		);
	}

}

?>
