<?php

/**
 * 
 * @author Rezan Achmad <rezan.achmad@gdpventure.com>
 */
class Selenium extends PHPUnit_Extensions_Selenium2TestCase
{
	/**
	 * In miliseconds
	 */
	const DELAY=500;
	
	const DATABASE_HOST='localhost';
	const DATABASE_USER='selenium';
	const DATABASE_PASSWORD='selenium';
	const DATABASE_NAME='selenium';
	
	const HOME_PAGE='http://localhost/selenium/';
	const REGISTER_PAGE='register/';
	const UPDATE_PROFILE_PAGE='user/update';
	const LOGIN_PAGE='site/login';

	protected function setUp()
	{
		parent::setUp();
		$this->setBrowser('firefox');
		$this->setBrowserUrl('http://localhost/');
	}

	public function cleanUpDB()
	{
		$config=array(
			'{{host}}'=>self::DATABASE_HOST,
			'{{database}}'=>self::DATABASE_NAME,
			'{{user}}'=>self::DATABASE_USER,
			'{{password}}'=>self::DATABASE_PASSWORD,
			'{{sqlDump}}'=>dirname(__FILE__).'/selenium.sql',
		);
		$command="mysql --compress --host={{host}} --database={{database}} --user={{user}} --password={{password}} < {{sqlDump}}";
		$command=str_replace(array_keys($config),array_values($config),$command);

		echo exec($command);
	}

	/**
	 * @param string $url
	 */
	public function urlRelative($url)
	{
		$this->url(self::HOME_PAGE.$url);
	}

	public function __call($command,$arguments)
	{
		usleep(self::DELAY*1000);
		return parent::__call($command,$arguments);
	}

}

?>
