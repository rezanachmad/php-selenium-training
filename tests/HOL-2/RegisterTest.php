<?php

require_once dirname(__FILE__).'/../Selenium.php';

/**
 * 
 *
 * @author Rezan Achmad <rezan.achmad@gdpventure.com>
 */
class RegisterTest extends Selenium
{

	/**
	 * @param string $formData
	 * @dataProvider providerSuccess
	 */
	public function testSuccess($formData)
	{
		$this->cleanUpDB();
		
		$this->url('http://localhost/selenium/');
		
		// $this->byLinkText('Register')->click();
		// TODO 01: Replace code above with using element()
		
		// $this->byId('User_fullname')->value($formData['fullname']);
		// TODO 02: Replace code above with using element()
		
		// $this->byName('User[email]')->value($formData['email']);
		// TODO 03: Replace code above with using element()
		
		// $this->byClassName('password')->value($formData['password']);
		// TODO 04: Replace code above with using element()
		
		// $this->byCssSelector('#User_retypePassword')->value($formData['retypePassword']);
		// TODO 05: Replace code above with using element()
		
		$this->element($this->using('id')->value('User_address'))->value($formData['address']);
		$this->select($this->element($this->using('id')->value('User_country')))->selectOptionByValue($formData['country']);
		if($formData['agree'])
		{
			$this->element($this->using('id')->value('User_agree'))->click();
		}
		$this->element($this->using('id')->value('submit'))->click();
		
		// Sleep to see the result
		sleep(5);
	}

	/**
	 * @return array
	 */
	public function providerSuccess()
	{
		return array(
			array(
				array(
					'fullname'=>'PHP Selenium',
					'email'=>'php.selenium@mail.com',
					'password'=>'password',
					'retypePassword'=>'password',
					'address'=>'Slipi',
					'country'=>'ID',
					'agree'=>true,
				),
			)
		);
	}

}

?>
