<?php

require_once dirname(__FILE__).'/../Selenium.php';
require_once dirname(__FILE__).'/../pages/RegisterPage.php';
require_once dirname(__FILE__).'/../pages/UpdateProfilePage.php';

/**
 * 
 *
 * @author Rezan Achmad <rezan.achmad@gdpventure.com>
 */
class RegisterTest extends Selenium
{

	/**
	 * @param string $formData
	 * @dataProvider providerSuccess
	 */
	public function testSuccess($formData)
	{
		$this->cleanUpDB();
		$this->urlRelative(self::REGISTER_PAGE);
		
		// TODO 00: First, please examine the implementation of RegisterPage, LoginPage and UpdateProfilePage in folder tests/pages/
		// Each of those represent 'Register' page, 'Login' page and 'Update Profile' page
		
		// TODO 01: create RegisterPage object, store it to $registerPage
		
		// TODO 02: call registerUser() from RegisterPage object & pass $formData
		//			and then store return value to $loginPage
		
		$this->assertEquals('Congratulations! You Have Successfully Registered.',$loginPage->getFlashSuccess());

		// TODO 03: call loginUser() from $loginPage object
		
		$this->urlRelative(self::UPDATE_PROFILE_PAGE);
		
		// TODO 04: create UpdateProfilePage object, store it to $profilePage
		
		// TODO 05: call verifyProfile() from UpdateProfilePage object & pass $formData
		
		$profilePage->logout();
	}

	/**
	 * @return array
	 */
	public function providerSuccess()
	{
		return array(
			array(
				array(
					RegisterPage::ID_EMAIL=>'php.selenium@mail.com',
					RegisterPage::ID_FULLNAME=>'PHP Selenium',
					RegisterPage::ID_PASSWORD=>'password',
					RegisterPage::ID_RETYPE_PASSWORD=>'password',
					RegisterPage::ID_ADDRESS=>'Slipi',
					RegisterPage::ID_COUNTRY=>'ID',
					RegisterPage::ID_AGREE=>true,
				),
			)
		);
	}

}

?>
