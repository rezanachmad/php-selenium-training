<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{

	/**
	 * Authenticates a user.
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate()
	{
		$user=User::model()->findByEmailAndPassword($this->username,$this->password);
		if ($user!==null)
		{
			$this->errorCode=self::ERROR_NONE;
			$this->setState('id',$user->id);
		}
		else
			$this->errorCode=self::ERROR_UNKNOWN_IDENTITY;

		return !$this->errorCode;
	}

}