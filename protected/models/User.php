<?php

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property string $id
 * @property string $fullname
 * @property string $email
 * @property string $password
 * @property string $address
 * @property string $country
 *
 * The followings are the available model relations:
 * @property Country $country
 */
class User extends CActiveRecord
{
	/**
	 * @var string form's property 
	 */
	public $retypePassword;

	/**
	 * @var bool form's property
	 */
	public $agree;

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('email', 'email'),
			array('email', 'unique'),
			array('fullname, address, country','required'),
			array('fullname, email','length','max'=>32),
			array('password, retypePassword','length','max'=>256),
			array('country','length','max'=>2),
			
			array('email, password, retypePassword, agree','required','on'=>'register'),
			array('retypePassword', 'compare', 'compareAttribute'=>'password','on'=>'register'),
			array('agree','required','requiredValue'=>'1','message'=>'{attribute} must be checked.','on'=>'register'),
			
			array('email', 'unsafe', 'on'=>'update'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'country'=>array(self::BELONGS_TO,'Country','country'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id'=>'ID',
			'fullname'=>'Fullname',
			'email'=>'Email',
			'password'=>'Password',
			'retypePassword'=>'Retype Password',
			'address'=>'Address',
			'country'=>'Country',
			'agree'=>'Agree with terms and conditions',
		);
	}

	protected function beforeSave()
	{
		if (!parent::beforeSave())
			return false;

		if ($this->scenario==='register')
			$this->password=self::hashPassword($this->password);
		
		return true;
	}

	/**
	 * @param string $email
	 * @param string $password
	 * @return User|null
	 */
	public function findByEmailAndPassword($email,$password)
	{
		$criteria=new CDbCriteria();
		$criteria->compare('email',$email);
		$criteria->compare('password',self::hashPassword($password));

		return $this->find($criteria);
	}

	/**
	 * @param string $password
	 * @return string
	 */
	public static function hashPassword($password)
	{
		return hash('sha256',$password);
	}

}