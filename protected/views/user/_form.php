<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */
?>

<div class="form">

	<?php
	$form=$this->beginWidget('CActiveForm',array(
		'id'=>'user-form',
		'enableAjaxValidation'=>false,
	));
	?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<div class="row">
		<?php echo $form->labelEx($model,'fullname'); ?>
		<?php echo $form->textField($model,'fullname',array('size'=>32,'maxlength'=>32)); ?>
		<?php echo $form->error($model,'fullname'); ?>
	</div>
	
	<div class="row">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php
		if ($model->scenario==='register')
		{
			echo $form->textField($model,'email',array('size'=>32,'maxlength'=>32));
			echo $form->error($model,'email');
		}
		else
		{
			echo CHtml::openTag('span', array('id'=>CHtml::activeId($model,'email')));
			echo $model->email;
			echo CHtml::closeTag('span');
		}
		?>
	</div>

	<?php if ($model->scenario==='register') : ?>
		<div class="row">
			<?php echo $form->labelEx($model,'password'); ?>
			<?php echo $form->passwordField($model,'password',array('size'=>32,'class'=>'password')); ?>
			<?php echo $form->error($model,'password'); ?>
		</div>

		<div class="row">
			<?php echo $form->labelEx($model,'retypePassword'); ?>
			<?php echo $form->passwordField($model,'retypePassword',array('size'=>32)); ?>
			<?php echo $form->error($model,'retypePassword'); ?>
		</div>
	<?php endif; ?>

	<div class="row">
		<?php echo $form->labelEx($model,'address'); ?>
		<?php echo $form->textArea($model,'address',array('rows'=>6,'cols'=>50)); ?>
		<?php echo $form->error($model,'address'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'country'); ?>
		<?php echo $form->dropDownList($model,'country',array(''=>'')+CHtml::listData(Country::model()->findAll(),'iso','name')) ?>
		<?php echo $form->error($model,'country'); ?>
	</div>

	<?php if ($model->scenario==='register') : ?>
		<div class="row">
			<?php echo $form->checkBox($model,'agree') ?>
			<span>Agree with terms & conditions</span>
			<?php echo $form->error($model,'agree'); ?>
		</div>
	<?php endif; ?>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Register' : 'Update', array('id'=>'submit', 'name'=>'submit')); ?>
	</div>

	<?php $this->endWidget(); ?>

</div><!-- form -->