<?php
/* @var $this UserController */
/* @var $model User */

$this->pageTitle=Yii::app()->name.' - Update Profile';
$this->breadcrumbs=array(
	'Update Profile',
);
?>

<h1>Update Profile</h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>

<script type="text/javascript">
	jQuery(function($) {
		$('#submit').click(function(e) {
			return confirm('Are you sure?');
		});
	});

</script>