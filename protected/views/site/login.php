<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name.' - Login';
$this->breadcrumbs=array(
	'Login',
);
?>

<h1>Login</h1>

<div class="form">
	<?php
	$form=$this->beginWidget('CActiveForm',array(
		'id'=>'login-form',
		'enableAjaxValidation'=>true,
		'clientOptions'=>array(
			'validateOnSubmit'=>true,
		),
	));
	?>

	<div class="row">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email'); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'password'); ?>
		<?php echo $form->passwordField($model,'password'); ?>
		<?php echo $form->error($model,'password'); ?>
	</div>

	<div class="row rememberMe">
		<?php echo $form->checkBox($model,'rememberMe'); ?>
		<?php echo $form->label($model,'rememberMe'); ?>
		<?php echo $form->error($model,'rememberMe'); ?>
	</div>

	<div class="row buttons">

	</div>

	<?php $this->endWidget(); ?>
</div><!-- form -->

<script type="text/javascript">
	jQuery(function($) {
		var buttonHtml = '<?php echo CHtml::submitButton('Login',array('id'=>'login')); ?>';
		
		function updateButton() {
			if ($('#LoginForm_email').val() !== '' && $('#LoginForm_password').val() !== '')
				$('.buttons').html(buttonHtml);
			else
				$('#login').remove();
		}

		$('#login-form').ready(function(){
			updateButton();
		});

		$('#LoginForm_email, #LoginForm_password').bind('focus keyup', function(e) {
			updateButton();
		});
	});
</script>
