INSTALATION
1. Put this folder to your http://localhost/selenium
2. Create database named 'selenium' in MySQL database
3. Add user 'selenium' with password 'selenium' to MySQL, and grant it privilege to database 'selenium'
4. Make sure these followings dirs are writable:
   - assets/
   - protected/runtime/

INSTRUCTION
1. Every Hands on Labs is store in tests/
2. Every Hands on Labs have solution, e.g. HOL-1 have solution HOL-Solution-1
3. Every Hands on Labs have TODOs.
   So, just search string 'TODO' to know what you must to do.

RUN TESTING
1. Open console/terminal, run selenium server
   java -jar selenium-server-standalon-xxx.jar
2. Open console/terminal, change directory (cd) to tests/
3. Run test
   phpunit --verbose --debug HOL-1/RegisterTest.php